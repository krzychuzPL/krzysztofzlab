## 1.
* **Project view** - wyświetla wszystkie pliki należące do projektu np. Assety
* **Hierarchy** - zawiera listę wszystkich Game Objectów w scenie
* **Scene view** - widok tworzonej sceny w którym można wybierać i ustawiać wszystkie rodzaje Game Objectów
* **Game view** - widok renderowany z pozycji kamery, jest prezentacją finalnej postaci gry
* **Console** - pokazuje błędy, ostrzeżenia i komunikaty generowane przez środowisko, oraz wiadomości wyświetlane przez nas z kodu (Debug.Log itp.)
* **Inspector** - wyświetla wszystkie informacje o wybranym obiekcie, w tym jego komponenty i ich właściwości

## 2. 
(w kolejności)

1. **Awake** - wywoływana tylko raz, gdy instancja skryptu jest ładowana (niezależnie od tego czy jest włączona); może służyć do zainicjalizowania skryptów; wywoływana przed jakimkolwiek wywołaniem Start w danej scenie
2. **OnEnable** - wywoływana gdy obiekt jest włączany
3. **Start** - wywoływana w momencie gdy skrypt jest włączany (enabled)
4. **FixedUpdate** - wywoływana okresowo niezależnie od ilosci fps; może służyć do obliczeń związanych z fizyką
5. **Update** - wywoływana dla każdej klatki
6. **LateUpdate** - wywoływana dla każdej klatki po wykonaniu wszystkich Update'ów

## 3.
Transform służy do ustawiania pozycji, rotacji i skali obiektów. Posiadają go wszystkieobiekty znajdujące się w scenie.

## 4.
GameObject jest klasą bazową wszystkich obiektów w scenie. Są to wszystkie podstawowe obiekty - postaci, rekwizyty i elementy scenerii. Same w sobie nie posiadają funkcjonalności ale uzyskują ją po przypisaniu do nich komponentów. 

## 5.
```csharp
Debug.Log("Something");
```

## 6.
`Time.deltaTime` zwraca czas jaki upłynął między poprzednią a obecną klatką. Możemy tej wartości użyć w metodzie `Update` do wykonywania obliczeń przy operacjach które mają być zależne od czasu a nie od ilości klatek na sekundę, która może się zmieniać. 
Np. chcąc jednostajne przesuwać jakiś obiekt trzeba mnożyć dystans przesunięcia przez tę wartość.

## 7.
Potrzebne są Collider i Rigidbody.

## 8.
Ten kod nie zadziała ponieważ getter `transform.position` zwraca kopię struktury `Vector3` przez wartość. Nie możemy w ten sposób przypisać nowej wartości do jej pól.

## 9.
Należy przypisać nową wartość typu `Vector3` do `transform.position`
```csharp
void Start()
{
    Vector3 pos = transform.position;
    pos.x = 10;
    transform.position = pos;
}
```

## 10.
Kontrola wersji używana jest aby śledzić zmiany w kodzie i nimi zarządzać, wspomaga ona róznież współpracę wielu programistów nad jednym kodem. Widoczna jest pełna historia zmian, możliwe jest rozgałęzianie aby pracować nad osobnymi funkcjonalnościami, można przywołać dowolną poprzednią wersję, cofnąć zmiany itd.